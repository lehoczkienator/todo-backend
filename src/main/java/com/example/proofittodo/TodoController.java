package com.example.proofittodo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TodoController {

    @Autowired
    private TodoRepository repository;

    @RequestMapping(method=RequestMethod.POST, path="/todos", produces="application/json", consumes="application/json")
    public ArrayList<Todo> createTodo(@RequestBody Todo todo) {
        repository.addTodo(todo);
        return repository.getTodos();
    }
    
    @RequestMapping(method=RequestMethod.GET, path="/todos", produces="application/json")
    public ArrayList<Todo> getTodos() {
        return repository.getTodos();
    }
    @RequestMapping(method=RequestMethod.DELETE, path="/todos/{id}", produces="application/json")
    public ArrayList<Todo> deleteTodo(@PathVariable String id) {
        repository.deleteTodo(id);
        return repository.getTodos();
    }

}

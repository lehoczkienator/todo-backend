package com.example.proofittodo;

import java.util.concurrent.atomic.AtomicLong;

public class Todo {
    public static AtomicLong counter = new AtomicLong();
    private Long id;
    private String content;
    private String date;

    public Todo(String content, String date) {
        this.content = content;
        this.date = date;
        this.id= counter.incrementAndGet();
    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "todo{" +
                "id='" + id + '\'' +
                ", content='" + content + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}

package com.example.proofittodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProofittodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProofittodoApplication.class, args);
	}
}

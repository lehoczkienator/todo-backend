package com.example.proofittodo;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class HelloController {

    @RequestMapping(method=RequestMethod.GET, path="/hello/{name}", produces="text/html")
    public String hello(@PathVariable String name) {
        System.out.println(name);
        return "Hello " + name + "!";
        
    }


}

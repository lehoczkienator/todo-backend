package com.example.proofittodo;

import java.util.stream.Collectors;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

@Repository
public class TodoRepository {

    private ArrayList<Todo> storage = new ArrayList<>();

    public void addTodo (Todo todo) {
        storage.add(0, todo);
    }

    public void deleteTodo (String id) {
        storage = (ArrayList<Todo>)storage.stream().filter((Todo todo) -> todo.getId() != Long.parseLong(id)).collect(Collectors.toList());
    }

    public ArrayList<Todo> getTodos () {
        return storage;
    }

}